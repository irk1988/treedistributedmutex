/*
 * Program to implement a client node in a tree based mutual exclusion algorithm
 * Client nodes will send request to the server nodes.
 * Server nodes send grants based on their state and request time.
 * Client enters a critical section if it recieves grant from a quorum of the tree
 *
 * Author: Philip Karunakaran, Immanuel Rajkumar
 * ixp140230
 */

#include<fstream>
#include<iostream>
#include<cstdlib>
#include<thread>
#include<sys/socket.h>
#include<netinet/in.h>
#include<netdb.h>
#include<unistd.h>
#include<vector>
#include<set>
#include<map>
#include<algorithm>
#include<cstring>
#include<mutex>
#include<ctime>
#include<sys/timeb.h>

#define SERVERS 7
#define PORT_NO 9877 //Base port no. i-th Server uses port serverId+1. Ex. Server 2 uses port 9879 
#define UNIT_TIME 100000 //Unit time in micro seconds - default 100 msec
#define CSCOUNT 10 //Number of times the client wants to enter critical section

using namespace std;

/* Address structure to store connection information about a server */
struct Address {
    int nodeId;
    string hostname;
    int port;
    int sockfd;
};

/*Lamport Clock*/
class LClock
{
public:
    int clock;
    int d;
    mutex clock_mu;

    LClock() {
        clock=0;
        d = 1;
    }

    void internalEvent() {
        clock_mu.lock();
        clock +=d;
        clock_mu.unlock();
    }

    void externalEvent( int extTime ) {
        clock_mu.lock();
        extTime > clock ? clock=extTime : clock+=d;
        clock_mu.unlock();
    }
};

/* Shared Messages List */
class Messages
{
private:
    vector<string> msgs;
    mutex msg_mu;
public:
    void add( string m ) {
        msg_mu.lock();
        msgs.push_back(m);
        msg_mu.unlock();
    }

    /* Return all the messages */
    vector<string> getAllMsgs() {
        return msgs;
    }
};

/*
 * Class to implement a client in a tree based mutual exclusion algorithm.
 * Client establishes connection with all the 7 server nodes. Send request to all these 7 servers when trying to enter critical section.
 * Based on grants recieved, checks if it has a grant from a total quorum and enters critical section if so.
 * Sends release message to all the servers on exiting the critical section.
 */
class ClientNode
{

private:
    int clientId;

    /*Msgs Logged in this output file*/
    char outfile[3];
    ofstream out;

    /* Array to store the connection information about each server */
    vector<Address> servers;

    /* Mutual Exclusion algorithm variables */
    LClock lclock; //Timestamps are used as sequence numbers while comparing requests
    bool quorumFormed;
    int attempt;
    set<int> failed;
    set<int> granted;
    set<int> inquired; //Inquired will be a subset of granted
    bool inquireRecvd;
    vector< set<int> > quorums; //Array of set of nodeIds. quorums[i] is a valid quorum set, say { 2,3,4 }
    mutex mtx; //for Thread safety
    mutex pmtx;
    vector<double> latency;

    bool start; //Flag that is set by server 1 via msgs to inform client can start competing for critical section.

    /* Messages */
    Messages sent;
    Messages recvd;
    map<int, int> grantsForCS;

public:
    /* Constructor to initialize various parameters of the client */
    ClientNode(int id) {
        clientId = id;
        sprintf(outfile,"C%d",clientId);
        out.open(outfile);
        start = false;
        quorumFormed = false;
        inquireRecvd = false;
        attempt = 0;
        getServerInfo();
        initQuorum();
    }

    /*
     * Retrieve server connection information from server info file : "InfoServers"
     */
    int getServerInfo() {
        ifstream ifile("InfoServers");
        string s;
        int i=1;
        if( ifile.is_open() ) {
            ifile.seekg(0, ios::beg);
            while( ifile >> s ) {
                Address server;
                server.nodeId = i;
                server.hostname = s;
                server.port = PORT_NO + i; //Required ??
                i++;
                servers.push_back(server);
            }
        }
        ifile.close();
        return 0;
    }

    /*
     * Initalize the tree-quorum.
     * Since the Quorum of the tree is defined explicitly and server nodes are of defined count, we enumerate the quorum.
     * Each Quorum is stored in a set. As the GRANTS are recieved from servers, we can compare the GRANT set and each of the quorum set.
     * Client can enter the critical section if atleast one of the set matches the GRANT set
     */
    int initQuorum() {
        set<int> q1 = {1,2,4};
        quorums.push_back(q1);
        set<int> q2 = {1,2,5};
        quorums.push_back(q2);
        set<int> q3 = {1,4,5};
        quorums.push_back(q3);
        set<int> q4 = {1,3,6};
        quorums.push_back(q4);
        set<int> q5 = {1,3,7};
        quorums.push_back(q5);
        set<int> q6 = {1,6,7};
        quorums.push_back(q6);
        set<int> q7 = {2,4,3,6};
        quorums.push_back(q7);
        set<int> q8 = {2,4,3,7};
        quorums.push_back(q8);
        set<int> q9 = {2,4,6,7};
        quorums.push_back(q9);
        set<int> q10 = {2,5,3,6};
        quorums.push_back(q10);
        set<int> q11 = {2,5,3,7};
        quorums.push_back(q11);
        set<int> q12 = {2,5,6,7};
        quorums.push_back(q12);
        set<int> q13 = {4,5,3,6};
        quorums.push_back(q13);
        set<int> q14 = {4,5,3,7};
        quorums.push_back(q14);
        set<int> q15 = {4,5,6,7};
        quorums.push_back(q15);
        return 0;
    }

    /* Routine to setup connections between all the 7 server nodes */
    int initConnections() {
        out<<"SETTING UP CONNECTIONS WITH ALL SERVERS"<<endl;
        for( int i=0; i<SERVERS; i++ )
            connectServer(servers[i]);

        /* Client now sends a READY msg to server 1. MSG TYPE - 0
         * Computation server - Server 1 will respond only if all the other clients are also READY
         * READY msg implies all the required connections are established by that client.
         * This is to ensure all the clients can start competing for resource at the same time and can help validate the algorithm
         */
        char msg[255];
        sprintf(msg,"%d-%d-0-%02d-%d", clientId, servers[0].nodeId, attempt, lclock.clock);
        send(servers[0].sockfd, msg, 255, 0);
        sent.add(msg);
        lclock.internalEvent();

        while(!start);

        thread csection(&ClientNode::startCriticalSection, this);
        csection.join();
        return 0;
    }

    /* Routine to connect to establish a TCP connection with a single server sent as a parameter */
    int connectServer( Address &s ) {
        int sockfd;
        struct sockaddr_in servaddr;
        struct hostent * server;

        sockfd = socket(AF_INET, SOCK_STREAM, 0);
        if( sockfd < 0 ) {
            out<<"ERROR OPENING SOCKET"<<endl;
            return 0;
        }

        server = gethostbyname(s.hostname.c_str());
        memset( &servaddr, 0, sizeof(servaddr));
        servaddr.sin_family = AF_INET;
        memcpy(&servaddr.sin_addr, server->h_addr_list[0], sizeof(server->h_addr_list[0]));
        servaddr.sin_port = htons(s.port);

        connect(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr));
        s.sockfd = sockfd;
        thread t(&ClientNode::handleServer, this, sockfd);
        t.detach();

        //Send NOTIFY msg to identify itself to the server. MSG TYPE - 1. Server uses this to identify the client and associated sockfd
        char msg[255];
        sprintf(msg,"%d-%d-1-%02d-%d",clientId, s.nodeId, attempt, lclock.clock);
        send(sockfd, msg, 255, 0);
        sent.add(msg);
        lclock.internalEvent();

        return 0;
    }

    /* Routine to handle each server connection. Waits for messages. On recieving one, send it to processMsg routine */
    int handleServer(int confd) {
        char buff[255];
        memset(buff, 0, sizeof(buff));
        while( read(confd, buff, 255) ) {
            mtx.lock();
            char mcopy[255];
            strcpy(mcopy, buff);
            recvd.add(mcopy);
            processMsg(mcopy, confd);
            mtx.unlock();
        }
        return 0;
    }

    /*
     * Routine to process messages sent as parameter
     * Types of message: GO, GRANT, INQUIRE, FAILED
     */
    int processMsg( char m[], int confd ) {
        /* START of thread safe code */
        int from=0;
        from = m[0] - '0';

        lclock.internalEvent();
        char stime[10];
        int i=0;
        for( i=9; m[i]!='\0'; i++ )
            stime[i-9] = m[i];
        stime[i] = '\0';
        int m_time = atoi(stime);
        lclock.externalEvent(m_time);

        int msg_attempt=0;
        char sAttm[3];
        for( i=6; i<=7; i++ )
            sAttm[i-6] = m[i];
        sAttm[i] = '\0';
        msg_attempt = atoi(sAttm);

        char msg[255];

        if( m[4] == '0' ) {
            //GO msg from server 1. Now client can start entering critical section
            start = true;
        } else if( m[4] == '1' ) {
            //GRANT msg from server - add it to grant set - remove from failed set
            from = m[0]-'0';
            //out<<"GRANT FROM: "<<from<<endl;
            if( attempt>msg_attempt ) {
                //out<<"GRANT FOR PREVIOUS REQUEST. CAN BE IGNORED"<<endl;
            } else {
                if( quorumFormed ) {
                   // out<<"QUORUM ALREADY FORMED. INSIDE CS - GRANT CAN BE IGNORED"<<endl;
                } else {
                    granted.insert(from);
                    set<int>::iterator d = failed.find(from);
                    if( d != failed.end() )
                        failed.erase(d);
                }
            }
        } else if( m[4] == '2' ) {
            //INQUIRE msg from server
            from = m[0]-'0';
            //out<<"INQUIRE FROM : "<<from<<endl;
            if( !quorumFormed ) {
                if( attempt == msg_attempt ) {
                    inquired.insert(from);
                    inquireRecvd = true;
                    //Check if I can enter CS from replies from other nodes. If I can't send a relinquish
                    if( !failed.empty() ) {
                        //Need to check if failed is a subset of all the quorums, if so I can't lock and send RELIQUISH to all INQUIRED nodes
                        bool cantLock = true;
			vector<bool> fchk(quorums.size(), false );
                        
			for(set<int>::iterator l = failed.begin(); l!=failed.end(); ++l ) {
                        	for( int i=0; i<quorums.size(); i++ ) {
					if( !fchk[i] ) {
						set<int>::iterator END = quorums[i].end();
						if( find( quorums[i].begin(), END, *l) != END ) {
							fchk[i] = true;
						}
					}
				}
			}
			
			vector<bool>::iterator l, FSTART=fchk.begin(), FEND=fchk.end();
			for( l=FSTART; l!=FEND; ++l )
				if( !(*l) ) {
					cantLock = false;
					break;
				}

                        if( cantLock ) {
                            for( set<int>::iterator k=granted.begin(); k!=granted.end(); ++k ) {
                                //out<<"******************QUORUM LOCK UNSUCCESSFUL - RELINQUISH SENT TO : "<<*k<<endl;
                                memset(msg, 0, sizeof(msg));
                                sprintf(msg,"%d-%d-4-%02d-%d",clientId, *k, attempt, lclock.clock);
                                send(servers[(*k)-1].sockfd, msg, 255, 0);
                                sent.add(msg);
                                lclock.internalEvent();
                            }
                            granted.clear();
                            inquired.clear();
                            inquireRecvd = false;
                        } else {
                            //out<<"FAILED SET NON-EMPTY - QUORUM LOCK YET TO BE FOUND - Reply/Relinquish deferred "<<endl;
                        }
                    } else {
                        //out<<"FAILED SET EMPTY - QUORUM LOCK YET TO BE FOUND - Reply/Relinquish deferred "<<endl;
		    }
                } else {
                    //out<<"INQUIRE FOR PREV REQ - Can be ignored "<<endl;
		}
            } else {
                //out<<"QUORUM ALREADY FORMED - INSIDE CS - RELEASE WILL BE SENT"<<endl;
            }
        } else if( m[4] == '3' ) {
            //FAILED msg from server
            from = m[0]-'0';
            //out<<"FAILED from: "<<from<<" -- in msg : "<<m[0]<<endl;
            if( attempt == msg_attempt ) {
                if( !quorumFormed ) {
                    failed.insert(from);
                    //Need to check if an inquire was sent to me and will I be able to lock other nodes to enter CS
                    if( inquireRecvd ) {
                        //out<<"INQUIRE ALREADY RECIEVED. NOW FAILED. CHECKING FOR LOCKABILITY."<<endl;
			bool cantLock = true;
			vector<bool> fchk(quorums.size(), false );
                        
			for(set<int>::iterator l = failed.begin(); l!=failed.end(); ++l ) {
                        	for( int i=0; i<quorums.size(); i++ ) {
					if( !fchk[i] ) {
						set<int>::iterator END = quorums[i].end();
						if( find( quorums[i].begin(), END, *l) != END ) 
							fchk[i] = true;
					}
				}
			}
			
			vector<bool>::iterator l, FSTART=fchk.begin(), FEND=fchk.end();
			for( l=FSTART; l!=FEND; ++l )
				if( !(*l) ) {
					cantLock = false;
					break;
				}

                        if( cantLock ) {
                            for( set<int>::iterator k=granted.begin(); k!=granted.end(); ++k ) {
                                //for( set<int>::iterator k=inquired.begin(); k!=inquired.end(); ++k ) {
                                //out<<"*****************QUORUM LOCK UNSUCCESSFUL - RELINQUISH sent to : "<<*k<<endl;
                                memset(msg, 0, sizeof(msg));
                                sprintf(msg,"%d-%d-4-%02d-%d",clientId, *k, attempt, lclock.clock);
                                send(servers[(*k)-1].sockfd, msg, 255, 0);
                                sent.add(msg);
                                lclock.internalEvent();
                            }
                            inquired.clear();
                            granted.clear();
                            inquireRecvd = false;
                        } else {
                            //out<<"LOCK CANNOT BE DETERMINED - FEW NODES IN FAILED SET - Deferred"<<endl;
                        }
                    }
                } else {
                    //out<<"QUORUM ALREADY FORMED. INSIDE CS. IGNORE FAILED"<<endl;
                }
            } else {
               //out<<"FAILED FOR PREVIOUS ATTEMPT. CAN BE IGNORED"<<endl;
            }
        }
        /* END of thread safe code */
        return 0;
    }

    /* Routine to start critical section attempts */
    int startCriticalSection() {
        char msg[255];
        for( int i=0; i<CSCOUNT; i++ ) {
            attempt = i+1;
            out<<endl<<"ATTEMPT : "<<attempt<<endl;
            usleep(UNIT_TIME* (5 + (rand() % (int)(10 - 5 + 1)))); //Sleep for random [5-10] UNIT TIME
            quorumFormed = false;
            clock_t startTime, endTime;

            struct timeb timebuffer1;
            char * timeline1;
            ftime(&timebuffer1);
            timeline1 = ctime(&(timebuffer1.time));
            out<<"CS REQ TIME : "<<timeline1<<"At Msec: "<<timebuffer1.millitm<<endl;
            //printf("REQ TIME : %.19s.%hu %s", timeline1, timebuffer1.millitm, &timeline1[20] );

            startTime = clock();
            for( int j=0; j<SERVERS; j++ ) {
                memset(msg, 0, sizeof(msg));
                sprintf(msg,"%d-%d-2-%02d-%d", clientId, servers[j].nodeId, attempt, lclock.clock);
                send(servers[j].sockfd, msg, 255, 0);
                sent.add(msg);
                lclock.internalEvent();
            }

            while(!quorumFormed) {
                quorumFormed = checkQuorumFormed(i+1); //Routine compares GRANT set with the list of valid quorum sets, returns true if match found
            }

            endTime = clock();
            double lat = (double)(endTime - startTime)/CLOCKS_PER_SEC*1000;
            latency.push_back(lat);

            struct timeb timebuffer;
            char * timeline;
            ftime(&timebuffer);
            timeline = ctime(&(timebuffer.time));
            out<<"ENTERING CS "<<i+1<<endl;
            out<<"ENTRY TIME : "<<timeline<<"At Msec: "<<timebuffer.millitm<<endl;
            //printf("ENTRY TIME : %.19s.%hu %s", timeline, timebuffer.millitm, &timeline[20] );

            usleep(3*UNIT_TIME); 
            //Clear the grant and failed set
            granted.clear();
            failed.clear();
            inquired.clear();
            inquireRecvd = false;

            //Send RELEASE to all the servers - MSG TYPE - 2
            for( int j=0; j<SERVERS; j++ ) {
                memset(msg, 0, sizeof(msg));
                sprintf(msg,"%d-%d-3-%02d-%d",clientId, servers[j].nodeId, attempt, lclock.clock);
                send(servers[j].sockfd, msg, 255, 0);
                sent.add(msg);
                lclock.internalEvent();
            }
        }

        //Send End of computation to server 1
        memset(msg, 0, sizeof(msg));
        sprintf(msg,"%d-1-5-%02d-%d",clientId, attempt, lclock.clock);
        send(servers[0].sockfd, msg, 255, 0);
        sent.add(msg);
        lclock.internalEvent();
	cout<<"END OF COMPUTATION"<<endl;
        return 0;
    }

    /* Routine compares the current GRANT set and list of valid quorums and returns true if a match found */
    bool checkQuorumFormed(int attempt) {
        int n = quorums.size();
        for( int i=0; i<n; i++ ) {
            if(includes(granted.begin(), granted.end(), quorums[i].begin(), quorums[i].end())) {
                //Quorums[i] is a subset of the granted
	    	grantsForCS[attempt] = granted.size();
                return true;
            }
        }
        return false;
    }

    /* Display all messages */
    int displayMessages() {
        map<int, int> sentCount, recvdCount;
        out<<"\nSENT MESSAGES : "<<endl;
        vector<string> sentMsgs = sent.getAllMsgs();
	map<int, int> perAttemptReq, perAttemptRep, perAttemptRel;
        for( vector<string>::iterator i=sentMsgs.begin(); i!=sentMsgs.end(); ++i ) {
            char strAttempt[3];
            strAttempt[0]=(*i).at(6);
            strAttempt[1]=(*i).at(7);
            strAttempt[2]='\0';
            int attempt = atoi(strAttempt);
            sentCount[attempt]++;
            
	    char stime[10]; int h=0; int len=(*i).size();
	    for( h=0; h+9<len; h++ ) 
		stime[h] = (*i).at(h+9);
	    stime[h] = '\0';
	    int timestamp = atoi(stime);

	    out<<"\tFrom: C"<<(*i).at(0)<<" To: S"<<(*i).at(2)<<" Type: ";
            if( (*i).at(4) == '0' ) out<<"Ready Notification ";
            else if( (*i).at(4) == '1' ) out<<"Connection Information ";
            else if( (*i).at(4) == '2' ) { out<<"Request "; perAttemptReq[attempt]++; }
            else if( (*i).at(4) == '3' ) { out<<"Release "; perAttemptRep[attempt]++; }
            else if( (*i).at(4) == '4' ) { out<<"Relinquish "; perAttemptRel[attempt]++; }
            out<<"Attempt: "<<(*i).at(6)<<(*i).at(7)<<" Timestamp: "<<timestamp<<endl;
        }

        out<<"\nRECVD MESSAGES : "<<endl;
        vector<string> recvdMsgs = recvd.getAllMsgs();
	map<int, int> perAttemptGr, perAttemptInq, perAttemptFl;
        for( vector<string>::iterator i=recvdMsgs.begin(); i!=recvdMsgs.end(); ++i ) {
            char strAttempt[3];
            strAttempt[0]=(*i).at(6);
            strAttempt[1]=(*i).at(7);
            strAttempt[2]='\0';
            int attempt = atoi(strAttempt);
	    
	    char stime[10]; int h=0; int len=(*i).size();
	    for( h=0; h+9<len; h++ ) 
		stime[h] = (*i).at(h+9);
	    stime[h] = '\0';
	    int timestamp = atoi(stime);

            recvdCount[attempt]++;
            out<<"\tFrom: S"<<(*i).at(0)<<" To: C"<<(*i).at(2)<<" Type: ";
            if( (*i).at(4) == '0' ) out<<"Initiate CS ";
            else if( (*i).at(4) == '1' ) { out<<"Grant "; perAttemptGr[attempt]++; }
            else if( (*i).at(4) == '2' ) { out<<"Inquire "; perAttemptInq[attempt]++; }
            else if( (*i).at(4) == '3' ) { out<<"Failed "; perAttemptFl[attempt]++; }
            out<<"Attempt: "<<(*i).at(6)<<(*i).at(7)<<" Timestamp: "<<timestamp<<endl;
        }

        out<<"\nLATENCY :"<<endl;
        for( vector<double>::iterator i=latency.begin(); i!=latency.end(); ++i ) {
            out<<"\tLAT: "<<*i<<" msec"<<endl;
        }

        out<<"\nMESSAGES EXCHANGED :"<<endl;
        out<<"SENT"<<endl;
	for(int i=0; i<CSCOUNT; i++ )
		out<<"\tAttempt:"<<i+1<<"-REQUEST: "<<perAttemptReq[i+1]<<" RELEASE: "<<perAttemptRep[i+1]<<" RELINQUISH: "<<perAttemptRel[i+1]<<endl;
        out<<"RECVD"<<endl;
	for(int i=0; i<CSCOUNT; i++ )
		out<<"\tAttempt: "<<i+1<<" - GRANT: "<<perAttemptGr[i+1]<<" INQUIRE: "<<perAttemptInq[i+1]<<" FAILED: "<<perAttemptFl[i+1]<<" GRANTS FOR CS ENTRY: "<<grantsForCS[i+1]<<endl;
	
        out.close();
    }
};

/* Main driver program - Argument is the single digit node Id of the client*/
int main(int argc, char **argv )
{
    int id=0;
    if( argc > 1 )
        id = atoi(argv[1]);
    cout<<"CLIENT NODE: "<<id<<endl;
    ClientNode cnode(id);
    cnode.initConnections();
    cnode.displayMessages();
    return 0;
}
