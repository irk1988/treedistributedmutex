/*
 * Program to implement a server node in a tree based mutual exclusion algorithm
 * Server nodes - 7 nodes - communicate with each client via deidicated TCP connections
 * Client nodes - Different program - will send request to these server nodes
 * Server nodes send grants to the requested clients based on its state and timestamp of the request
 *
 * Author: Philip Karunakaran, Immanuel Rajkumar
 * ixp140230
 */

#include<fstream>
#include<iostream>
#include<cstdlib>
#include<thread>
#include<sys/socket.h>
#include<netinet/in.h>
#include<netdb.h>
#include<unistd.h>
#include<unistd.h>
#include<cstring>
#include<vector>
#include<queue>
#include<mutex>
#include<set>

#define CLIENTS 5
#define SERVERS 7
#define PORT_NO 9877 //Server node communicate via port = (port+id) Ex. Server Node 2 uses port 9877+2=9879

#define COMPSERVER "irk-Ultrabook"

using namespace std;

/* Address structure to store connection information about a client */
struct Address {
    int nodeId;
    int sockfd;
};

/*Lamport Clock*/
class LClock
{
public:
    int clock;
    int d;
    mutex clock_mu;

    LClock() {
        clock=0;
        d = 1;
    }

    void internalEvent() {
        clock_mu.lock();
        clock +=d;
        clock_mu.unlock();
    }

    void externalEvent( int extTime ) {
        clock_mu.lock();
        extTime > clock ? clock=extTime : clock+=d;
        clock_mu.unlock();
    }
};

/*
 * Class to implement a server in the distributed system.
 * Listens to incoming connections from clients. Sets a new thread for handling each client connection.
 * Contains a queue to line up the request from clients.
 */
class ServerNode
{
    int nodeId;
    /*Connection Listening Port Variables */
    int listeningPortNo;
    int listenfd;

    /*Msgs Logged in this output file*/
    char outfile[3];
    ofstream out;
    LClock lclock;

    /* Array to store the connection information about each client */
    vector<Address> clients;
    vector<Address> servers;
    int compServerSockfd;

    /* Algorithm variables */
    bool locked;
    int curReqTimestamp;
    int curGrantNode;
    int curReqAttempt;
    bool inquireSent;
    int completed;
    mutex mtx; //Used for thread safety wherever required

    /* WAITING queue maintained by the server. Request are <nodeId,timeStamp> pair and sorted by timeStamp */
    struct QElement {
        int nodeId;
        int timestamp;
        int attempt;

        bool operator < ( const QElement &r ) const {
            if( timestamp == r.timestamp )
                return nodeId < r.nodeId; //Resolving timestamp conflict with nodeIds
            else
                return timestamp < r.timestamp;
        }
    };
    set<QElement> waitingQ;

    /*Computation start/stop varibles*/
    int readyCount; //Number of clients ready to compete for CSection

public:
    /* Constructor to initialize various parameters of the server node */
    ServerNode( int id ) {
        nodeId = id;
        listeningPortNo = PORT_NO + id;
        sprintf(outfile,"S%d",nodeId); //Outputfile name set as the nodeid
        out.open(outfile);
        readyCount = 0;
        locked = false;
        curReqTimestamp = 0;
        curGrantNode = 0;
        inquireSent = false;
        compServerSockfd = 0;
        completed = 0; //No of clients done computation. Used for termination

        //Populate client information. Sockfd for each client will be added on recieving NOTIFY msgs
        for( int i=0; i<CLIENTS; i++ ) {
            Address client;
            client.nodeId = i+1;
            client.sockfd = 0;
            clients.push_back(client);
        }

        if( nodeId == 1 ) {
            for( int i=0; i< SERVERS; i++ ) {
                Address server;
                server.nodeId = i+1;
                server.sockfd = 0;
                servers.push_back(server);
            }
        }
    }

    /* Routine to connect to Computation Manager Server - In this case Server 1 */
    int connectPeer() {
        if( nodeId != 1 ) {
            out<<"CONNECT TO COMPUTATION MGR SERVER"<<endl;
            int sockfd;
            struct sockaddr_in servaddr;
            struct hostent * server;

            sockfd = socket(AF_INET, SOCK_STREAM, 0);
            if( sockfd < 0 ) {
                out<<"ERROR OPENING SOCKET"<<endl;
                return 0;
            }

            char cserver[] = COMPSERVER;
            server = gethostbyname(cserver);
            memset( &servaddr, 0, sizeof(servaddr));
            servaddr.sin_family = AF_INET;
            memcpy(&servaddr.sin_addr, server->h_addr_list[0], sizeof(server->h_addr_list[0]));
            servaddr.sin_port = htons(PORT_NO+1);

            connect(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr));
            compServerSockfd = sockfd;
            thread t(&ServerNode::handleClient, this, sockfd);
            t.detach();

            //Send NOTIFY msg to computation servers (server 1) to identify itself. MSG TYPE - 9
            char msg[255];
            sprintf(msg,"%d-1-9-00-%d",nodeId, lclock.clock);
            send(compServerSockfd, msg, 255, 0);
            lclock.internalEvent();
        }
        return 0;
    }

    /* Routine to listen to incoming connections and handle them by spawning a new thread for each connection */
    int initListener(void) {
        connectPeer(); //Connect to computation server (server1) first
        out<<"SETTING UP LISTENER"<<endl;
        listenfd = socket(AF_INET, SOCK_STREAM, 0);
        if( listenfd < 0 ) {
            out<<"ERROR OPENING SOCKET"<<endl;
            return 0;
        }
        struct sockaddr_in serv_addr, client_addr;
        socklen_t caddr_len;

        memset(&serv_addr, 0, sizeof(serv_addr));
        serv_addr.sin_family = AF_INET;
        serv_addr.sin_addr.s_addr = htonl(INADDR_ANY); //accept connection from any interface
        serv_addr.sin_port = htons(listeningPortNo);

        bind(listenfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr));
        listen(listenfd, 10);
        caddr_len = sizeof(client_addr);

        int confd = 0;
        while(1) {
            confd = accept(listenfd, (struct sockaddr *)&client_addr, &caddr_len);
            //Handle each connection with a new thread
            thread t(&ServerNode::handleClient, this, confd);
            t.detach();
        }
        close(listenfd);
        return 0;
    }

    /* Routine to handle each client connection. Ideally, 5 connections to be handled, one for each client. */
    int handleClient(int confd) {
        char buff[255];
        memset(buff, 0, sizeof(buff));
        while( read(confd, buff, 255) ) {
            mtx.lock();
            char mcopy[255];
            strcpy(mcopy, buff);
            processMsg(mcopy, confd);
            mtx.unlock();
        }
        return 0;
    }

    /* Routine to handle each message from any client */
    int processMsg( char m[], int confd ) {
        /* BEGIN - Thread safe code */
        char msg[255];
        int from=0;
        from = m[0] - '0';

        lclock.internalEvent();
        char stime[10];
        int i=0;
        for( i=9; m[i]!='\0'; i++ )
            stime[i-9] = m[i];
        stime[i] = '\0';
        int timestamp = atoi(stime);
        lclock.externalEvent(timestamp);

        int attempt=0;
        char sAttm[3];
        for( i=6; i<=7; i++ )
            sAttm[i-6] = m[i];
        sAttm[i] = '\0';
        attempt = atoi(sAttm);

        if( m[4] == '9' ) {
            //Identification Message to Computation server from other peer servers. Uses this to note the sockfd of peer servers
            out<<"SERVER-SERVER IDENTIFICATION MSG FROM SERVER: "<<from<<endl;
            servers[from-1].sockfd = confd;
        } else if( m[4] == '0' ) {
            //READY msg - Will be recieved by only server 1 - the designated computation start/stop node.
            readyCount++;
            if( readyCount == CLIENTS ) {
                //All the clients have sent READY msg. Clients can now fairly compete for CS. Send GO msg to all the clients
                for( int i=CLIENTS-1; i>=0; i-- ) {
                    memset(msg, 0, sizeof(msg));
                    sprintf(msg,"%d-%d-0-00-%d",nodeId, clients[i].nodeId, lclock.clock);
                    send(clients[i].sockfd, msg, 255, 0);
                    lclock.internalEvent();
                }
            }
        } else if( m[4] == '1' ) {
            //NOTIFY msg - This will be sent by each client once to identify itself(store sockfd) to the server
            clients[from-1].sockfd = confd;
        } else if( m[4] == '2' ) {
            //REQUEST msg
            out<<"REQ FROM: "<<from<<endl;
            if( !locked ) {
                //Resource free -  can be provided to the requester
                if( !waitingQ.empty() ) {
                    locked = true;
                    QElement nextReq;
                    set<QElement>::iterator beg = waitingQ.begin();
                    nextReq = (*beg);
                    waitingQ.erase(beg);
                    curReqTimestamp = nextReq.timestamp;
                    curGrantNode = nextReq.nodeId;
                    curReqAttempt = nextReq.attempt;
                    out<<"RESOURCE FREE - PQ NONEMPTY - GRANTING TO NEXT IN QUEUE: "<<curGrantNode<<endl;
                } else {
                    locked = true;
                    curReqTimestamp = timestamp;
                    curGrantNode = from;
                    curReqAttempt = attempt;
                    out<<"RESOURCE FREE - PQ EMPTY - GRANTING TO CURRENT REQUESTER: "<<curGrantNode<<endl;
                }
                //Send GRANT  - MSG TYPE 1
                memset(msg, 0, sizeof(msg));
                sprintf(msg,"%d-%d-1-%02d-%d",nodeId, curGrantNode, curReqAttempt, lclock.clock);
                send(clients[curGrantNode-1].sockfd, msg, 255, 0);
                lclock.internalEvent();
            } else  {
                //Resource already locked - Check if inquire needs to be sent
                if( timestamp < curReqTimestamp ) {
                    if( !inquireSent ) {
                        out<<"LOCKED-SENDING INQ-REQ PRECEDES CURREQ-REQ_TS :"<<timestamp<<" CURPROCESSING_TS:"<<curReqTimestamp<<endl;
                        //If inquire not sent - send now - MSG TYPE 2
                        memset(msg, 0, sizeof(msg));
                        sprintf(msg,"%d-%d-2-%02d-%d",nodeId, curGrantNode, attempt, lclock.clock);
                        send(clients[curGrantNode-1].sockfd, msg, 255, 0);
                        lclock.internalEvent();
                        inquireSent = true;
                    } else {
                        out<<"LOCKED-INQ ALREADY SENT-REQ PRECEDES CURREQ-REQTS :"<<timestamp<<" CURPROCESSING_TS:"<<curReqTimestamp<<endl;
                        //Inquire already sent - So just add the request to the queue
                    }
                } else if( timestamp == curReqTimestamp ) {
                    //Need to resolve with the nodeId
                    if( from < curGrantNode ) {
                        if(!inquireSent) {
                            out<<"SAME TS-SENDING INQ- REQ PRECEDES CURREQ-REQ_TS :"<<timestamp<<" CURPROCESSING_TS:"<<curReqTimestamp<<endl;
                            memset(msg, 0, sizeof(msg));
                            sprintf(msg,"%d-%d-2-%02d-%d",nodeId, curGrantNode, attempt, lclock.clock);
                            send(clients[curGrantNode-1].sockfd, msg, 255, 0);
                            lclock.internalEvent();
                            inquireSent = true;
                        } else {
                            out<<"SAME TS-INQUIRE ALREADY SENT- REQ_TS:"<<timestamp<<" CURPROCESSING_TS:"<<curReqTimestamp<<endl;
                            //Inquire already sent - So we can add this request to the queue
                        }
                    } else {
                        //Current request precedes the incoming request - So place it in queue - send failed msg - MSG TYPE 3
                        out<<"SAME TS-IDCHK-PROCREQ PRECEDES CURREQ-SENDING FAILED-REQTS:"<<timestamp<<" CURPROCESSING_TS:"<<curReqTimestamp<<endl;
                        memset(msg, 0, sizeof(msg));
                        sprintf(msg,"%d-%d-3-%02d-%d",nodeId, from, attempt, lclock.clock);
                        send(clients[from-1].sockfd, msg, 255, 0);
                        lclock.internalEvent();
                    }
                } else if( timestamp > curReqTimestamp ) {
                    //Current request precedes the incoming request - So place it in queue - send failed msg - MSG TYPE 3
                    out<<"SENDING FAILED TO:"<<from<<" PROCESSINGREQ PRECEDES CURREQ-REQ_TS:"<<timestamp<<" PROCESSING_TS:"<<curReqTimestamp<<endl;
                    memset(msg, 0, sizeof(msg));
                    sprintf(msg,"%d-%d-3-%02d-%d",nodeId, from, attempt, lclock.clock);
                    send(clients[from-1].sockfd, msg, 255, 0);
                    lclock.internalEvent();
                }
                //Add the request to the queue
                QElement req;
                req.nodeId = from;
                req.timestamp = timestamp;
                req.attempt = attempt;
                waitingQ.insert(req);
            }

        } else if( m[4] == '3' ) {
            //RELEASE msg
            out<<"RELEASE FROM: "<<from<<endl;
            if( from == curGrantNode ) {
                //Node which got permission completed its CS. Grant to next in queue
                if( waitingQ.empty() )
                    locked = false; //Request queue empty - set server state as unlocked
                else {
                    QElement nextReq;
                    set<QElement>::iterator beg = waitingQ.begin();
                    nextReq = (*beg);
                    waitingQ.erase(beg);
                    curReqTimestamp = nextReq.timestamp;
                    curGrantNode = nextReq.nodeId;
                    curReqAttempt = nextReq.attempt;
                    //Send GRANT to next in queue
                    out<<"SENDING GRANT TO NEXT IN THE Q : "<<nextReq.nodeId<<endl;
                    memset(msg, 0, sizeof(msg));
                    sprintf(msg,"%d-%d-1-%02d-%d",nodeId, nextReq.nodeId, curReqAttempt, lclock.clock);
                    send(clients[nextReq.nodeId-1].sockfd, msg, 255, 0);
                    lclock.internalEvent();
                }
                //Also if this is a release from a node that an inquire was sent to. Then I can revoke the INQUIRE.
                inquireSent = false;
            } else {
                //Some other node completed its CS. Remove it from the queue
                set<QElement>::iterator z;
                for( z=waitingQ.begin(); z!=waitingQ.end(); ) {
                    if((*z).nodeId == from )
                        waitingQ.erase(z++);
                    else
                        ++z;
                }
            }
        } else if( m[4] == '4' ) {
            // RELINQUISH msg
            out<<"**********RELINQUISH FROM : "<<from<<endl;
            if( from == curGrantNode ) {
                if( !waitingQ.empty() ) {
                    inquireSent = false;
                    //Move the current executing node back to queue
                    QElement cur;
                    cur.nodeId = curGrantNode;
                    cur.timestamp = curReqTimestamp;
                    cur.attempt = curReqAttempt;
                    waitingQ.insert(cur);

                    QElement nextReq;
                    set<QElement>::iterator beg = waitingQ.begin();
                    nextReq = (*beg);
                    waitingQ.erase(beg);
                    curGrantNode = nextReq.nodeId;
                    curReqTimestamp = nextReq.timestamp;
                    curReqAttempt = nextReq.attempt;
                    //Send GRANT to next in queue
                    out<<"SENDING GRANT TO NEXT IN THE Q : "<<nextReq.nodeId<<endl;
                    memset(msg, 0, sizeof(msg));
                    sprintf(msg,"%d-%d-1-%02d-%d", nodeId, nextReq.nodeId, curReqAttempt, lclock.clock);
                    send(clients[nextReq.nodeId-1].sockfd, msg, 255, 0);
                    lclock.internalEvent();
                } else {
                    out<<"NO REQS IN THE Q. SENDING GRANT TO CURRENT NODE AGAIN."<<from<<endl;
                    out<<"GRANT TO CURRENT NODE AGAIN : "<<curGrantNode<<endl;
                    memset(msg, 0, sizeof(msg));
                    sprintf(msg,"%d-%d-1-%02d-%d", nodeId, curGrantNode, curReqAttempt, lclock.clock);
                    send(clients[curGrantNode-1].sockfd, msg, 255, 0);
                    lclock.internalEvent();
                }
            } else {
                out<<"******RELINQUISH FROM A NONCURRENT NODE. NEED NOT BOTHER AS ITS REQ WILL BE IN QUEUE"<<endl;
            }
        } else if( m[4] == '5' ) {
            //END OF COMPUTATION FROM SERVERS 2-N TO SERVER 1
            out<<"END OF COMPUTATION from : "<<from<<endl;
            completed++;
            if( completed == CLIENTS ) {
                //Send closure to all the servers - MSG TYPE 6
                for( int i=1; i<SERVERS; i++ ) {
                    memset(msg, 0, sizeof(msg));
                    sprintf(msg,"%d-%d-6-00-%d", nodeId, servers[i].nodeId, lclock.clock);
                    send(servers[i].sockfd, msg, 255, 0);
                    lclock.internalEvent();
                }
                out<<"COMPUTATION TERMINATED SUCESSFULLY"<<endl;
                exit(0);
            }
        } else if( m[4] == '6' ) {
            //TERMINATE message will be recieved by server 2-N from server 1
            out<<"TERMINATE RECIEVED FROM SERVER "<<from<<endl;
            cout<<"TERMINATE RECIEVED FROM SERVER "<<from<<endl;
	    out.close();
            exit(0);
        }

        /* END - Thread safe code */
        return 0;
    }
};

/* Main driver routine - Argument is the single digit node Id of the server */
int main(int argc, char **argv)
{
    int nodeId = 0;
    if( argc > 1 )
        nodeId = atoi(argv[1]);
    cout<<"SERVER NODE: "<<nodeId<<endl;
    ServerNode snode(nodeId);
    snode.initListener();
    return 0;
}
